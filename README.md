# Movieland 

React + Redux + RTK + Bootstrap application that fetches movies from [https://www.themoviedb.org/](https://www.themoviedb.org/)

Created with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm install`

Install all dependencies

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

---------------------------

PR comments:

- to have all code equally aligned and to avoid merge conflicts - .prettierrc.js file needed as well as prettier module installed.
"npm i prettier". All rules should be discussed within a team.
- to avoid crossed overwriting styles, I would recommend to use CSS modules stylesheets. It should also be discussed with a team.
- API calls in App component can be replaced to custom hook and imported to that component 

- Header component:
  - "starredMovies" can be extracted in useSelector instead of whole state.
  - input element needs value and onChange()

- Movie component:
  - "closeCard" prop is redundant and is not in use;
  - There is no need to extract whole state. We can select "{starred, watchLater}" only by using useSelector hook and passing shallowEqual as we return new created object.
  - id can be extracted and simplified in callback by mapping "starredMovies" and "watchLaterMovies"
  - By dispatching unstarMovie action, it would be enough to pass just movie.id, instead of passing whole object movie;
  - there is no need to pass whole movie object into viewTrailer() function. Just id will be enough. As a result - small changes in viewTrailer() in <App> required.

- Starred component: 
  - There is no need to extract whole state if we use only "starredMovies". We can select "starredMovies" only by using useSelector hook.
- WatchLater component:
  - not big deal, just 1 letter is missing in naming "removeAllWatchLater"
  - There is no need to extract whole state if we use only "watchLaterMovies". We can select "watchLaterMovies" only by using useSelector hook and passing shallowEqual. We could also create separate file for selectors only when project get bigger.
- App component:
  - No need to select whole state and to use just "movies". movies can be selected by useSelector hook. 
  - isOpen is not in use as well as closeModal() fn.



