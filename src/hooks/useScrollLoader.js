import {useEffect, useState} from 'react';
import moviesSlice, {fetchMovies} from '../data/moviesSlice';
import {API_KEY, ENDPOINT, ENDPOINT_DISCOVER, ENDPOINT_SEARCH} from '../constants';
import {useDispatch, useSelector} from 'react-redux';
import {createSearchParams, useNavigate, useSearchParams} from 'react-router-dom';

export default function useScrollLoader() {
  const [searchParams, setSearchParams] = useSearchParams();
  const searchQuery = searchParams.get('search');
  const moviesData = useSelector(({movies}) => movies.movies);
  const {results: movies = [], success = 'true'} = moviesData;

  const [currentPage, setCurrentPage] = useState(1);
  const [isFetching, setIsFetching] = useState(true);

  const {combineAllMovies} = moviesSlice.actions;

  const scrollHandler = (e) => {
    if (e.target.documentElement.scrollHeight - (e.target.documentElement.scrollTop + window.innerHeight) < 100) {
      setIsFetching(true);
    }
  };

  const [isOpen, setOpen] = useState(false);

  const navigate = useNavigate();
  const [videoKey, setVideoKey] = useState();
  const dispatch = useDispatch();

  const bodyScrollingHandler = () => {
    if (isOpen) {
      document.body.classList.add('active-modal');
    } else {
      document.body.classList.remove('active-modal');
    }
  };

  const getMovies = () => {
    if (searchQuery) {
      dispatch(fetchMovies(`${ENDPOINT_SEARCH}&query=` + searchQuery));
    } else {
      dispatch(fetchMovies(`${ENDPOINT_DISCOVER}&page=` + currentPage));
      setCurrentPage((prevPage) => prevPage + 1);
      setIsFetching(false);
    }
  };

  const getMovie = async (id) => {
    const URL = `${ENDPOINT}/movie/${id}?api_key=${API_KEY}&append_to_response=videos`;

    setVideoKey(null);
    const videoData = await fetch(URL).then((response) => response.json());

    if (videoData?.videos && videoData.videos.results.length) {
      const trailer = videoData.videos.results.find((vid) => vid.type === 'Trailer');
      setVideoKey(trailer ? trailer.key : videoData.videos.results[0].key);
    }
  };

  const getSearchResults = (query) => {
    if (query !== '') {
      dispatch(fetchMovies(`${ENDPOINT_SEARCH}&query=` + query));
      setSearchParams(createSearchParams({search: query}));
    } else {
      dispatch(fetchMovies(ENDPOINT_DISCOVER));
      setSearchParams();
    }
  };

  const searchMovies = (query) => {
    navigate('/');
    getSearchResults(query);
  };

  const viewTrailer = (movieId) => {
    getMovie(movieId);
    if (!videoKey) setOpen(true);
    setOpen(true);
  };

  useEffect(() => {
    document.addEventListener('scroll', scrollHandler);
    return () => {
      document.removeEventListener('scroll', scrollHandler);
    };
  }, []);

  useEffect(() => {
    if (isFetching && success) {
      getMovies();
      dispatch(combineAllMovies(movies));
    }
  }, [isFetching, movies]);

  useEffect(() => {
    bodyScrollingHandler();
  }, [isOpen]);

  return {
    isOpen,
    setOpen,
    viewTrailer,
    videoKey,
    searchMovies,
    searchParams,
    setSearchParams,
    movies,
    success,
  };
}
