import {Routes, Route} from 'react-router-dom';
import {createPortal} from 'react-dom';
import useScrollLoader from './hooks/useScrollLoader';
import Header from './components/Header';
import Movies from './components/Movies';
import Starred from './components/Starred';
import WatchLater from './components/WatchLater';
import Modal from './components/Modal';
import './app.scss';

const App = () => {
  const {isOpen, setOpen, viewTrailer, videoKey, searchMovies, searchParams, setSearchParams, movies, success} =
    useScrollLoader();

  return (
    <div className="App">
      <Header searchMovies={searchMovies} searchParams={searchParams} setSearchParams={setSearchParams} />
      <div className="container">
        <Routes>
          <Route path="/" element={<Movies movies={movies} success={success} viewTrailer={viewTrailer} />} />
          <Route path="/starred" element={<Starred viewTrailer={viewTrailer} />} />
          <Route path="/watch-later" element={<WatchLater viewTrailer={viewTrailer} />} />
          <Route path="*" element={<h1 className="not-found">Page Not Found</h1>} />
        </Routes>
      </div>
      {isOpen && createPortal(<Modal isOpen={isOpen} setOpen={setOpen} videoKey={videoKey} />, document.body)}
    </div>
  );
};

export default App;
