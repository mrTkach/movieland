import {Link, NavLink} from 'react-router-dom';
import {useSelector} from 'react-redux';

import '../styles/header.scss';
import {useState} from 'react';

const Header = ({searchMovies}) => {
  const starredMovies = useSelector(({starred}) => starred.starredMovies);
  const [searchValue, setSearchValue] = useState('');

  const clickHandler = (e) => {
    searchMovies('');
    setSearchValue('');
  };
  return (
    <header>
      <Link to="/" data-testid="home" onClick={clickHandler}>
        <i className="bi bi-film" />
      </Link>

      <nav>
        <NavLink to="/starred" data-testid="nav-starred" className="nav-starred">
          {starredMovies.length > 0 ? (
            <>
              <i className="bi bi-star-fill bi-star-fill-white" />
              <sup className="star-number">{starredMovies.length}</sup>
            </>
          ) : (
            <i className="bi bi-star" />
          )}
        </NavLink>

        <NavLink to="/watch-later" className="nav-fav">
          watch later
        </NavLink>
      </nav>

      <div className="input-group rounded">
        <Link to="/" onClick={clickHandler} className="search-link">
          <input
            value={searchValue}
            type="search"
            data-testid="search-movies"
            onKeyUp={(e) => searchMovies(e.target.value)}
            onChange={(e) => setSearchValue(e.target.value)}
            className="form-control rounded"
            placeholder="Search movies..."
            aria-label="Search movies"
            aria-describedby="search-addon"
          />
        </Link>
      </div>
    </header>
  );
};

export default Header;
