import React from 'react';
import YouTubePlayer from '../components/YoutubePlayer';
import styles from '../styles/modal.module.scss';

const Modal = ({setOpen, videoKey}) => {
  return (
    <div className={styles.modalPopup}>
      <div onClick={() => setOpen(false)} className={styles.overlayLayout} />
      <div className={styles.modalContent}>
        {videoKey ? (
          <YouTubePlayer videoKey={videoKey} />
        ) : (
          <div style={{padding: '30px'}}>
            <h6>No trailer available. Try another movie</h6>
          </div>
        )}
        <button className={styles.closeModal} onClick={() => setOpen(false)}>
          Close
        </button>
      </div>
    </div>
  );
};

export default Modal;
