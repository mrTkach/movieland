import Movie from './Movie';
import '../styles/movies.scss';

import {useSelector} from 'react-redux';

const Movies = ({success, movies, viewTrailer, closeCard}) => {
  const combinedMovies = useSelector(({movies}) => movies.combinedMovies);
  let allMovies = !combinedMovies.length ? movies : combinedMovies;

  if (!success) {
    return <p>The resource you requested could not be found.</p>;
  }

  return (
    <div data-testid="movies" className="movies">
      {allMovies?.map((movie) => (
        <Movie movie={movie} viewTrailer={viewTrailer} closeCard={closeCard} />
      ))}
    </div>
  );
};

export default Movies;
